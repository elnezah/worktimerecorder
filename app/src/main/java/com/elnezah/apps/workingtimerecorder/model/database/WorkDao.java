package com.elnezah.apps.workingtimerecorder.model.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

// Grâce à Room, le code du DAO sera généré à partir des annotations

@Dao
public interface WorkDao {

    @Insert
    void insert(Work work);

    @Delete
    void delete(Work work);

    @Update
    void update(Work work);

   @Query("DELETE FROM work")
    void deleteAllWorks();

   @Query("select * from work order by startTime desc")
   LiveData<List<Work>> getAllWorks();
}
