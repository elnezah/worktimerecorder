package com.elnezah.apps.workingtimerecorder.model.database;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

// tableName est le nom de la table SQLite
@Entity(tableName = "work",
        foreignKeys = {
                @ForeignKey(entity = Project.class, parentColumns = "id", childColumns = "project", onDelete = ForeignKey.CASCADE),
                @ForeignKey(entity = Location.class, parentColumns = "id", childColumns = "place", onDelete = ForeignKey.CASCADE)}
)
public class Work {


    /*
    @Entity(tableName = "panos",
indices = {@Index(value = {"tour_title"})},
foreignKeys = @ForeignKey(entity = TourEntity.class,
parentColumns = "title",
childColumns = "tour_title",
onDelete = ForeignKey.CASCADE))
     */

    @PrimaryKey(autoGenerate = true)
    private int id;
    private long startTime;
    private long endTime;
    private int project;
    private int place;
    private long rest;
    private String comments;

    public Work(int id, long startTime, long endTime, int project, int place, long rest, String comments) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.project = project;
        this.place = place;
        this.rest = rest;
        this.comments = comments;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public int getProject() {
        return project;
    }

    public void setProject(int project) {
        this.project = project;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public long getRest() {
        return rest;
    }

    public void setRest(long rest) {
        this.rest = rest;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
