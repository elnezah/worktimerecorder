package com.elnezah.apps.workingtimerecorder.model.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

@Database(entities = {Project.class, Location.class, Work.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public abstract ProjectDao projectDao();
    public abstract WorkDao workDao();
    public abstract LocationDao locationDao();

    public static synchronized AppDatabase getInstance(Context context){
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class,"app_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
        }
    };
/*
    private static class PopulateDbAsyncTask extends AsyncTask<Void,Void,Void>{
        private ProjectDao projectDao;

        public PopulateDbAsyncTask(AppDatabase db) {
            projectDao = db.projectDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            projectDao.insert(new Project("Project 1", "Description 1", 1));
            projectDao.insert(new Project("Project 2", "Description 2", 2));
            projectDao.insert(new Project("Project 3", "Description 3", 3));
            return null;
        }
    }*/
}
