package com.elnezah.apps.workingtimerecorder;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.elnezah.apps.workingtimerecorder.model.database.Project;
import com.elnezah.apps.workingtimerecorder.viewmodel.WorkViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final int ADD_NOTE_REQUEST = 1;
    public static final int EDIT_NOTE_REQUEST = 2;

    private WorkViewModel workViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // bouton flottant pour créer une Project
        FloatingActionButton buttonAddNote = findViewById(R.id.button_add_note);
        buttonAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddEditNoteActivity.class);
                startActivityForResult(intent, ADD_NOTE_REQUEST);
            }
        });

        // on définit le RecyclerView
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        // et on lui affecte l'adapteur de Project
        final NoteAdapter noteAdapter = new NoteAdapter();
        recyclerView.setAdapter(noteAdapter);


        // on récupère le ViewModel
        workViewModel = ViewModelProviders.of(this).get(WorkViewModel.class);
        // on s'abonne aux mises à jour des données
        workViewModel.getAllNotes().observe(this, new Observer<List<Project>>() {
            @Override
            public void onChanged(@Nullable List<Project> projects) {
                // maintenant, quand les données changent (dans la BDD),
                // on demande à l'adapteur de mettre à jour la liste de projects à l'écran

                // ancien code avant ListAdapter
                // noteAdapter.setNotes(projects);
                // nouveau code après ListAdapter
                noteAdapter.submitList(projects);
            }
        });

        // ajout de la fonctionnalité de swipe pour supprimer une Project
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                workViewModel.delete(noteAdapter.getNoteAt(viewHolder.getAdapterPosition()));
                Toast.makeText(MainActivity.this, "Project deleted", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerView);  // obligatoire pour que le swipe fonctionne !

        // ajout de la gestion du clic sur une note dans le RecyclerView
        noteAdapter.setOnItemClickListener(new NoteAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Project project) {
                Intent intent = new Intent(MainActivity.this, AddEditNoteActivity.class);
                intent.putExtra(AddEditNoteActivity.EXTRA_ID, project.getId());
                intent.putExtra(AddEditNoteActivity.EXTRA_TITLE, project.getTitle());
                intent.putExtra(AddEditNoteActivity.EXTRA_DESCRIPTION, project.getDescription());
                intent.putExtra(AddEditNoteActivity.EXTRA_PRIORITY, project.getPriority());
                startActivityForResult(intent, EDIT_NOTE_REQUEST);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Si on a créé une note et que tout est OK, on sauve la note en utilisant le ViewModel
        if (requestCode == ADD_NOTE_REQUEST && resultCode == RESULT_OK) {
            String title = data.getStringExtra(AddEditNoteActivity.EXTRA_TITLE);
            String description = data.getStringExtra(AddEditNoteActivity.EXTRA_DESCRIPTION);
            int priority = data.getIntExtra(AddEditNoteActivity.EXTRA_PRIORITY, 1);

            Project project = new Project(title, description, priority);
            workViewModel.insert(project);

            Toast.makeText(this, "Project saved", Toast.LENGTH_SHORT).show();
        }
        // sinon, si on a édité une note et que tout est OK, on met à jour la note
        else if (requestCode == EDIT_NOTE_REQUEST && resultCode == RESULT_OK) {
            int id = data.getIntExtra(AddEditNoteActivity.EXTRA_ID,-1);
            
            // Ce cas ne devrait jamais arriver, mais géré au as où !
            if (id == -1) {
                Toast.makeText(this, "Project can't be updated", Toast.LENGTH_SHORT).show();
                return;
            }

            String title = data.getStringExtra(AddEditNoteActivity.EXTRA_TITLE);
            String description = data.getStringExtra(AddEditNoteActivity.EXTRA_DESCRIPTION);
            int priority = data.getIntExtra(AddEditNoteActivity.EXTRA_PRIORITY, 1);

            Project project = new Project(title, description, priority);
            project.setId(id);
            workViewModel.update(project);

            Toast.makeText(this, "Project updated", Toast.LENGTH_SHORT).show();

        } else
        // l'utilisateur a fait back (donc RESULT_CANCELED)
        {
            Toast.makeText(this, "Project not saved", Toast.LENGTH_SHORT).show();
        }
    }


    // On ajoute le menu 'delete all notes' et sa gestion du clic
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = new MenuInflater(this);
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_all_notes:
                workViewModel.deleteAllNotes();
                Toast.makeText(this, "All notes deleted", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
