package com.elnezah.apps.workingtimerecorder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

/*
    Actvité pour remplir le détail d'une note
 */

public class AddEditNoteActivity extends AppCompatActivity {

    /*
        on va utiliser un startActivityForResult pour communiquer entre l'ajout
        d'une note et l'activité principale, donc on crée des clés pour renvoyer
        les valeurs. Pour cela, une bonne pratique est d'utiliser le nom complet
        du package pour éviter d'éventuels doublons
     */
    public static final String EXTRA_ID =
            "com.example.jeanclaude.room_livedata_mvvm.EXTRA_ID";
    public static final String EXTRA_TITLE =
            "com.example.jeanclaude.room_livedata_mvvm.EXTRA_TITLE";
    public static final String EXTRA_DESCRIPTION =
            "com.example.jeanclaude.room_livedata_mvvm.EXTRA_DESCRIPTION";
    public static final String EXTRA_PRIORITY =
            "com.example.jeanclaude.room_livedata_mvvm.EXTRA_PRIORITY";


    private EditText editTextTitle;
    private EditText editTextDescription;
    private NumberPicker numberPickerPriority;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        editTextTitle = findViewById(R.id.edit_text_title);
        editTextDescription = findViewById(R.id.edit_text_description);
        numberPickerPriority = findViewById(R.id.number_picker_priority);
        numberPickerPriority.setMinValue(1);
        numberPickerPriority.setMaxValue(10);

        // on ajoute l'icône de fermeture dans la Toolbar, ainsi que le titre de l'activité
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        // on vérifie si on est en mode Add Project ou Edit Project
        Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_ID)) {
            setTitle("Edit Project");
            editTextTitle.setText(intent.getStringExtra(EXTRA_TITLE));
            editTextDescription.setText(intent.getStringExtra(EXTRA_DESCRIPTION));
            numberPickerPriority.setValue(intent.getIntExtra(EXTRA_PRIORITY,1));
        } else {
            setTitle("Add Project");
        }
    }

    // on ajoute le menu pour sauver une Project, dans la ToolBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_note_menu, menu);
        return true;
    }

    // on gère le clic dans le menu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_note:
                saveNote();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // on sauve la note saisie (si non vide)
    private void saveNote() {
        String title = editTextTitle.getText().toString();
        String description = editTextDescription.getText().toString();
        int priority = numberPickerPriority.getValue();

        if (title.trim().isEmpty() || description.trim().isEmpty()) {
            Toast.makeText(this, "Please insert a title and a description", Toast.LENGTH_LONG).show();
            return;
        }

        // si la note est non vide, on récupère ses valeurs et on les renvoie à
        // l'actitivité principale (d'autres façons de faire sont bien sûr possible)
        Intent data = new Intent();
        data.putExtra(EXTRA_TITLE, title);
        data.putExtra(EXTRA_DESCRIPTION, description);
        data.putExtra(EXTRA_PRIORITY, priority);

        // maintenant, cette activité crée OU édite une note. Dans le cas de l'édition,
        // on doit donc ajouter la sauvegarde de l'ID de la note éditée
        int id = getIntent().getIntExtra(EXTRA_ID,-1);
        if (id != -1)
        {
            data.putExtra(EXTRA_ID, id);
        }

        setResult(RESULT_OK, data);
        finish();
    }
}