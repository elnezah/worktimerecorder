package com.elnezah.apps.workingtimerecorder.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.elnezah.apps.workingtimerecorder.model.AppRepository;
import com.elnezah.apps.workingtimerecorder.model.database.Project;

import java.util.List;

public class WorkViewModel extends AndroidViewModel {

    private AppRepository repository;
    private LiveData<List<Project>> allNotes;

    // Ne JAMAIS mettre une activité ou un fragment en contexte ici, mais TOUJOURS l'application
    // puisque le ViewModel doit toujours être accessible même si l'activité est détruite par Android
    // (donc perte du contexte Activity !)
    public WorkViewModel(@NonNull Application application) {
        super(application);
        repository = new AppRepository(application);
        allNotes = repository.getAllProjects();
    }

    // méthodes pour CRUD des Notes

    public void insert (Project project){
        repository.insert(project);
    }

    public void update(Project project){
        repository.update(project);
    }

    public void delete (Project project){
        repository.delete(project);
    }

    public void deleteAllNotes(){
        repository.deleteAllNotes();
    }

    public LiveData<List<Project>> getAllNotes() {
        return allNotes;
    }
}
