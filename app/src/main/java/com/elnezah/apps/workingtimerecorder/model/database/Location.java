package com.elnezah.apps.workingtimerecorder.model.database;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

// tableName est le nom de la table SQLite
@Entity(tableName = "location")
public class Location {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String telephoneNumber;
    private String address;

    public Location(int id, String name, String telephoneNumber, String address) {
        this.id = id;
        this.name = name;
        this.telephoneNumber = telephoneNumber;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
