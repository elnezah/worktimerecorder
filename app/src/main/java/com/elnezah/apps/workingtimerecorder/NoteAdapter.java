package com.elnezah.apps.workingtimerecorder;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elnezah.apps.workingtimerecorder.model.database.Project;

// Modification de la classe mère pour permettre des animations en cas de modifications du contenu
// du RecyclerView. ListAdapter permet en effet de savoir où a été inséré, supprimé, modifié un élément,
// et par conséquent de créer une animation pour montrer le résultat à et endroit.
// On doit créer le constructeur associé à diffCallback.
public class NoteAdapter extends ListAdapter<Project, NoteAdapter.NoteHolder> {

    private OnItemClickListener listener;

    // on n'a plus besoin de la liste de notes
    // private List<Project> notes = new ArrayList<>();

    // on remplace protected (généré de base) par public et on modifie le code.
    // on passe ainsi de
    //    protected NoteAdapter(@NonNull DiffUtil.ItemCallback<Project> diffCallback) {
    //        super(diffCallback);
    //    }
    // à
    public NoteAdapter() {
        super(DIFF_CALLBACK);
    }

    // méthodes qui permettent d'établir les différences entre les données avant et après modification/insertion/suppression...
    public static final DiffUtil.ItemCallback<Project> DIFF_CALLBACK = new DiffUtil.ItemCallback<Project>() {
        @Override
        public boolean areItemsTheSame(@NonNull Project oldItem, @NonNull Project newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Project oldItem, @NonNull Project newItem) {
            // Mauvaise solution car retournera toujours false puisque ce ne sont pas le même objets java !
            // return oldItem.equals(newItem);
            // bonne solution : tous les champs sont identiques
            return oldItem.getTitle().equals(newItem.getTitle())
                    && oldItem.getDescription().equals(newItem.getDescription())
                    && oldItem.getPriority() == newItem.getPriority();
        }
    };


    // crée la vue graphique d'un élément de la liste du RecyclerView
    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.note_item, viewGroup, false);
        return new NoteHolder(itemView);
    }

    // on attache un NoteHolder à une note
    @Override
    public void onBindViewHolder(@NonNull NoteHolder noteHolder, int position) {
        // ancien code avant ListAdapter
        // Project currentProject = notes.get(position);
        // nouveau code après ListAdapter
        Project currentProject = getItem(position);
        noteHolder.textViewTitle.setText(currentProject.getTitle());
        noteHolder.textViewDescription.setText(currentProject.getDescription());
        noteHolder.textViewPriority.setText(String.valueOf(currentProject.getPriority()));
    }

    // Code devenu inutile avec ListAdapter
    //    @Override
    //    public int getItemCount() {
    //        return notes.size();
    //    }

    // Code devenu inutile avec ListAdapter
    //    public void setNotes(List<Project> notes) {
    //        this.notes = notes;
    //        // solution simple pour le moment pour mettre à jour les données, mais on changera ça + tard
    //        // en utilisant des notifyItemInserted/Changed/... pour gérer ça + proprement et joliment avec
    //        // des animations par exemple
    //        notifyDataSetChanged();
    //    }

    // méthode qui renvoie la position d'une Project dans le ViewHolder
    // méthode utilisée par MainActivity pour permettre la suppression d'une Project
    // en faisant un swipe dessus.
    public Project getNoteAt(int position) {
        // ancien code avant ListAdapter
        // return notes.get(position);
        // nouveau code après ListAdapter
        return getItem(position);
    }


    // classe qui connecte chaque élément du Recyclerview à une note
    class NoteHolder extends RecyclerView.ViewHolder {

        private TextView textViewTitle;
        private TextView textViewDescription;
        private TextView textViewPriority;

        public NoteHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewDescription = itemView.findViewById(R.id.text_view_description);
            textViewPriority = itemView.findViewById(R.id.text_view_priority);

            // gestion du clic sur une Project (CarView)
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    // on vérifie qu'on a bien un clic sur une vraie position
                    // (si on a supprimé une note auparavant, on évite ainsi un index = -1 !!!)
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        // ancien code avant ListAdapter
                        // listener.onItemClick(notes.get(position));
                        // nouveau code après ListAdapter
                        listener.onItemClick(getItem(position));
                    }
                }
            });
        }
    }

    // interface pour gérer le clic sur une Project pour sa mise à jour
    public interface OnItemClickListener {
        void onItemClick(Project project);
    }

    // définition du clic sur un élément du RecyclerView
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
