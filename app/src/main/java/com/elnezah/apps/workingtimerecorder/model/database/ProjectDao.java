package com.elnezah.apps.workingtimerecorder.model.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ProjectDao {

    @Insert
    void insert(Project project);

    @Delete
    void delete(Project project);

    @Update
    void update(Project project);

   @Query("DELETE FROM project")
    void deleteAllProjects();

   @Query("select * from project order by name asc")
   LiveData<List<Project>> getAllProjects();
}
