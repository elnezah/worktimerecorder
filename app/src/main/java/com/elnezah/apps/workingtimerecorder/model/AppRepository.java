package com.elnezah.apps.workingtimerecorder.model;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.elnezah.apps.workingtimerecorder.model.database.Location;
import com.elnezah.apps.workingtimerecorder.model.database.LocationDao;
import com.elnezah.apps.workingtimerecorder.model.database.Project;
import com.elnezah.apps.workingtimerecorder.model.database.ProjectDao;
import com.elnezah.apps.workingtimerecorder.model.database.AppDatabase;
import com.elnezah.apps.workingtimerecorder.model.database.Work;
import com.elnezah.apps.workingtimerecorder.model.database.WorkDao;

import java.util.List;

public class AppRepository {

    private ProjectDao projectDao;
    private LocationDao locationDao;
    private WorkDao workDao;

    private LiveData<List<Project>> allProjects;
    private LiveData<List<Location>> allLocations;
    private LiveData<List<Work>> allWorks;

    public AppRepository(Application application) {

        AppDatabase database = AppDatabase.getInstance(application);

        projectDao = database.projectDao();
        locationDao = database.locationDao();
        workDao = database.workDao();

        allProjects = projectDao.getAllProjects();
        allLocations = locationDao.getAllLocations();
        allWorks = workDao.getAllWorks();

    }

    public void insert(Project project){
        new InsertNoteAsyncTask(projectDao).execute(project);
    }

    public void update(Project project){
        new UpdateNoteAsyncTask(projectDao).execute(project);
    }

    public void delete(Project project){
        new DeleteNoteAsyncTask(projectDao).execute(project);
    }

    public void deleteAllNotes(){
        new DeleteAllNotesAsyncTask(projectDao).execute();
    }

    public LiveData<List<Project>> getAllProjects() {
        return allProjects;
    }

    private static class InsertNoteAsyncTask extends AsyncTask<Project, Void, Void>{
        private ProjectDao projectDao;

        private InsertNoteAsyncTask(ProjectDao projectDao) {
            this.projectDao = projectDao;
        }

        @Override
        protected Void doInBackground(Project... projects) {
            projectDao.insert(projects[0]);
            return null;
        }
    }

    private static class UpdateNoteAsyncTask extends AsyncTask<Project, Void, Void>{
        private ProjectDao projectDao;

        private UpdateNoteAsyncTask(ProjectDao projectDao) {
            this.projectDao = projectDao;
        }

        @Override
        protected Void doInBackground(Project... projects) {
            projectDao.update(projects[0]);
            return null;
        }
    }

    private static class DeleteNoteAsyncTask extends AsyncTask<Project, Void, Void>{
        private ProjectDao projectDao;

        private DeleteNoteAsyncTask(ProjectDao projectDao) {
            this.projectDao = projectDao;
        }

        @Override
        protected Void doInBackground(Project... projects) {
            projectDao.delete(projects[0]);
            return null;
        }
    }

    private static class DeleteAllNotesAsyncTask extends AsyncTask<Void, Void, Void>{
        private ProjectDao projectDao;

        private DeleteAllNotesAsyncTask(ProjectDao projectDao) {
            this.projectDao = projectDao;
        }

        @Override
        protected Void doInBackground(Void...voids) {
            projectDao.deleteAllProjects();
            return null;
        }
    }
}
