package com.elnezah.apps.workingtimerecorder.model.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface LocationDao {

    @Insert
    void insert(Location location);

    @Delete
    void delete(Location location);

    @Update
    void update(Location location);

   @Query("DELETE FROM location")
    void deleteAllLocations();

   @Query("select * from location order by name asc")
   LiveData<List<Location>> getAllLocations();
}
